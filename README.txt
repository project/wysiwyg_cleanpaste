This module was created by Haymarket Media Group by DMT team
Developer: Mihhail Arhipov (drupal.org id: skein)

Install
-------
1) Put the module directory into sites/all/modules
folder in your Drupal installation.

2) Enable the module in admin/modules or using drush.

3) Go to admin/config/content/wysiwyg.

4) Select a Text Format with CKEDITOR and Edit it.

5) In Buttons and Plugins check "Change div tags to p tags on paste." and save.

6) Now when using this editor when you paste text into it it will change div
to p tags.
