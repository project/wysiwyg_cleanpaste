/**
 * @file
 * Plugin to clean the pasted text in a CKEDITOR.
 */
(function () {
  'use strict';
  CKEDITOR.plugins.add('cleanpaste', {
    init: function (editor) {
      editor.on('paste', function (e) {
        if (e.data.html) {
          e.data.html = e.data.html
            .replace(/<div/g, '<p')
            .replace(/<\/\s{0,}div>/g, '</p>')
            .replace(/<p><br\s{0,}\/?><\/p>/g, '');
        }
      });
    }
  });
})();
